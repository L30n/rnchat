import { Card, CardItem, Content, Fab, Icon, Text } from 'native-base'
import React, { Component } from 'react'
import { AsyncStorage } from 'react-native'
import QRCode from 'react-native-qrcode'
import QRCodeScanner from 'react-native-qrcode-scanner'
import RNSigntx from 'react-native-signtx'
import {
  createStackNavigator,
  NavigationScreenProp,
  NavigationState,
} from 'react-navigation'
import { NavScreen } from '../NavScreen'

export class HomeScreen extends Component<any, any> {
  constructor(props: any) {
    super(props)
    this.state = {
      privateKey: '',
      contract: '',
      message: 'loading...',
    }
  }

  async componentDidMount() {
    AsyncStorage.getItem('privateKey').then(value =>
      this.setState({ privateKey: value })
    )
    AsyncStorage.getItem('contract').then(value =>
      this.setState({ contract: value })
    )
    let message = 'dd'
    message = await RNSigntx.sayHello()
    this.setState({
      message,
    })
  }

  getResult = (qr: string) => {
    const t = qr.substr(0, 2)
    switch (t) {
      case 'FF':
        AsyncStorage.setItem('privateKey', qr.substr(2))
        this.setState({ privateKey: qr.substr(2) })
        break
      case 'FE':
        AsyncStorage.setItem('contract', qr.substr(2))
        this.setState({ contract: qr.substr(2) })
        break
    }
  }

  render() {
    return (
      <NavScreen banner="Home Tab" navigation={this.props.navigation}>
        <Content>
          <Card style={{ flex: 0 }}>
            <CardItem header={true} bordered={true}>
              <QRCode
                value={
                  this.props.navigation.state.params
                    ? this.props.navigation.state.params.qr
                    : 'qr'
                }
                size={300}
                bgColor="purple"
                fgColor="white"
              />
            </CardItem>
            <CardItem footer={true}>
              <Text>合约</Text>
            </CardItem>
            <CardItem>
              <Text>{this.state.contract}</Text>
            </CardItem>
            <CardItem footer={true}>
              <Text>密钥</Text>
            </CardItem>
            <CardItem>
              <Text>{this.state.privateKey}</Text>
              <Text>{this.state.message}</Text>
            </CardItem>
          </Card>
        </Content>
        <Fab
          containerStyle={{ left: '40%' }}
          style={{ backgroundColor: '#5067FF' }}
          position="bottomLeft"
          onPress={() =>
            this.props.navigation.navigate('Scanner', {
              name: 'QR',
              result: this.getResult,
            })
          }
        >
          <Icon name="ios-qr-scanner" />
        </Fab>
      </NavScreen>
    )
  }
}

export class ScanScreen extends Component<{ navigation: any }> {
  constructor(props: any) {
    super(props)
  }

  onSuccess = (e: { data: string }) => {
    this.props.navigation.state.params.result(e.data)
    this.props.navigation.navigate('ScannerMain', {
      qr: e.data,
    })
    alert(e.data)
  }

  render() {
    return <QRCodeScanner onRead={this.onSuccess} />
  }
}

export const ScannerTab = createStackNavigator({
  ScannerMain: {
    navigationOptions: {
      header: null,
    },
    path: '/scnnermain',
    screen: HomeScreen,
  },
  Scanner: {
    navigationOptions: ({
      navigation,
    }: {
      navigation: NavigationScreenProp<NavigationState>
    }) => ({
      title: `${navigation.state.params.name}`,
    }),
    path: '/scanner',
    screen: ScanScreen,
  },
})
