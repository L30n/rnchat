import { Button, Content, Icon, SwipeRow, Text, View } from 'native-base'
import React from 'react'
import { FlatList, SafeAreaView } from 'react-native'
import { GiftedChat } from 'react-native-gifted-chat'
import {
  createStackNavigator,
  NavigationScreenProp,
  NavigationState,
} from 'react-navigation'
import { NavScreen } from '../NavScreen'
const datas = [
  { key: '1', value: '联系人1' },
  { key: '2', value: '联系人2' },
  { key: '3', value: '群聊1' },
  { key: '4', value: '联系人3' },
  { key: '5', value: '群聊2' },
]

export class SwipeableList extends React.Component<any, { data: any }> {
  constructor(props: any) {
    super(props)
    this.state = { data: datas }
  }
  removeItem(key: any) {
    let data = this.state.data
    data = data.filter((item: { key: any }) => item.key !== key)
    this.setState({ data })
  }

  render() {
    return (
      <Content scrollEnabled={false}>
        <FlatList
          data={this.state.data}
          renderItem={({ item }: { item: { key: any; value: any } }) => (
            <SwipeRow
              leftOpenValue={75}
              rightOpenValue={-75}
              left={
                <Button success={true} onPress={() => alert(item.value)}>
                  <Icon active={true} name="information-circle" />
                </Button>
              }
              body={
                <Button
                  full={true}
                  onPress={() =>
                    this.props.navigation.navigate('ChatBox', {
                      name: item.value,
                    })
                  }
                >
                  <Text>{item.value}</Text>
                </Button>
              }
              right={
                <Button danger={true} onPress={() => this.removeItem(item.key)}>
                  <Icon active={true} name="trash" />
                </Button>
              }
            />
          )}
        />
      </Content>
    )
  }
}

export class ChatBox extends React.Component<{}, { messages: any }> {
  componentWillMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Hello developer',
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://placeimg.com/140/140/any',
          },
        },
      ],
    })
  }

  onSend(messages: any = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <GiftedChat
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: 1,
          }}
        />
      </SafeAreaView>
    )
  }
}

const ChatListScreen = ({
  navigation,
}: {
  navigation: NavigationScreenProp<NavigationState>
}) => (
  <NavScreen banner="Chat Tab" navigation={navigation}>
    <SwipeableList navigation={navigation} />
  </NavScreen>
)

export const ChatTab = createStackNavigator({
  ChatList: {
    navigationOptions: {
      header: null,
    },
    path: '/chatlist',
    screen: ChatListScreen,
  },
  ChatBox: {
    navigationOptions: ({
      navigation,
    }: {
      navigation: NavigationScreenProp<NavigationState>
    }) => ({
      title: `${navigation.state.params.name}`,
    }),
    path: '/chatbox/:name',
    screen: ChatBox,
  },
})
