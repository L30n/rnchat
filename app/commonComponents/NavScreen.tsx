import {
  Body,
  Button,
  Container,
  Header,
  Icon,
  Left,
  Right,
  Title,
} from 'native-base'
import React from 'react'

export const NavScreen = (props: any) => {
  return (
    <Container>
      <Header>
        <Left>
          <Button transparent={true}>
            <Icon name="menu" onPress={() => props.navigation.toggleDrawer()} />
          </Button>
        </Left>
        <Body>
          <Title>{props.banner}</Title>
        </Body>
        <Right />
      </Header>
      {props.children}
    </Container>
  )
}
