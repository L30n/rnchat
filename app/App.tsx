import { Button, Content, Header, Right, Text } from 'native-base'
import React from 'react'
import { ImageBackground } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import {
  createAppContainer,
  createBottomTabNavigator,
  createDrawerNavigator,
  createStackNavigator,
  NavigationActions,
  NavigationScreenProp,
  NavigationState,
  SafeAreaView,
  StackActions,
} from 'react-navigation'
import { ChatTab } from './commonComponents/ChatBox/ChatBox'
import { NavScreen } from './commonComponents/NavScreen'
import { ScannerTab } from './commonComponents/QrScanner/QrScanner'

const Ctn = (props: any) => (
  <Content padder={true}>
    <Button onPress={() => props.navigation.navigate('Chat')}>
      <Text>Chat</Text>
    </Button>
    <Button onPress={() => props.navigation.navigate('Home')}>
      <Text>Home</Text>
    </Button>
    <Button
      transparent={true}
      onPress={() => props.navigation.navigate('People')}
    >
      <Text>People</Text>
    </Button>
    <Button
      transparent={true}
      onPress={() => props.navigation.navigate('Settings')}
    >
      <Text>Settings</Text>
    </Button>
    <Button transparent={true} onPress={() => props.navigation.goBack(null)}>
      <Text>Back</Text>
    </Button>
  </Content>
)

const PeopleScreen = ({
  navigation,
}: {
  navigation: NavigationScreenProp<NavigationState>
}) => (
  <NavScreen banner="People Tab" navigation={navigation}>
    <Ctn navigation={navigation} />
  </NavScreen>
)

const SettingsScreen = ({
  navigation,
}: {
  navigation: NavigationScreenProp<NavigationState>
}) => (
  <NavScreen banner="Settings Tab" navigation={navigation}>
    <Ctn navigation={navigation} />
  </NavScreen>
)

const ModalScreen = ({
  navigation,
}: {
  navigation: NavigationScreenProp<NavigationState>
}) => {
  const resetHome = () => {
    navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Main' })],
      })
    )
  }
  return (
    <ImageBackground
      source={require('./assets/night-sky.png')}
      style={{ width: '100%', height: '100%' }}
    >
      <SafeAreaView>
        <Header transparent={true}>
          <Right>
            <Button transparent={true} light={true} onPress={resetHome}>
              <Text>跳过</Text>
            </Button>
          </Right>
        </Header>
      </SafeAreaView>
    </ImageBackground>
  )
}

const MainStack = createBottomTabNavigator(
  {
    Chat: {
      navigationOptions: ({
        navigation,
      }: {
        navigation: NavigationScreenProp<NavigationState>
      }) => {
        let tabBarVisible = true
        if (navigation.state.index > 0) {
          tabBarVisible = false
        }
        return {
          tabBarVisible,
          tabBarIcon: ({
            tintColor,
            horizontal,
          }: {
            tintColor: string
            horizontal: boolean
          }) => (
            <Ionicons
              name={'ios-chatboxes'}
              size={horizontal ? 20 : 26}
              style={{ color: tintColor }}
            />
          ),
          tabBarLabel: 'Chat',
        }
      },
      path: 'chat',
      screen: ChatTab,
    },
    Home: {
      navigationOptions: {
        tabBarIcon: ({
          tintColor,
          horizontal,
        }: {
          tintColor: string
          horizontal: boolean
        }) => (
          <Ionicons
            name={'ios-home'}
            size={horizontal ? 20 : 26}
            style={{ color: tintColor }}
          />
        ),
        tabBarLabel: 'Home',
        tabBarTestIDProps: {
          accessibilityLabel: 'TEST_ID_HOME_ACLBL',
          testID: 'TEST_ID_HOME',
        },
      },
      path: '',
      screen: ScannerTab,
    },
    People: {
      navigationOptions: {
        tabBarIcon: ({
          tintColor,
          focused,
          horizontal,
        }: {
          tintColor: string
          focused: boolean
          horizontal: boolean
        }) => (
          <Ionicons
            name={'ios-people'}
            size={horizontal ? 20 : 26}
            style={{ color: tintColor }}
          />
        ),
        tabBarLabel: 'People',
      },
      path: 'cart',
      screen: PeopleScreen,
    },
    Settings: {
      navigationOptions: {
        tabBarIcon: ({
          tintColor,
          focused,
          horizontal,
        }: {
          tintColor: string
          focused: boolean
          horizontal: boolean
        }) => (
          <Ionicons
            name={'ios-settings'}
            size={horizontal ? 20 : 26}
            style={{ color: tintColor }}
          />
        ),
        tabBarLabel: 'Settings',
      },
      path: 'settings',
      screen: SettingsScreen,
    },
  },
  {
    backBehavior: 'history',
    tabBarOptions: {
      activeTintColor: '#e91e63',
    },
  }
)

const DrawerScreen = ({
  navigation,
}: {
  navigation: NavigationScreenProp<NavigationState>
}) => <NavScreen banner="Drawer" navigation={navigation} />

const DrawerMenu = createDrawerNavigator(
  {
    MainStack: {
      navigationOptions: {
        drawerLabel: () => null,
      },
      screen: MainStack,
    },
    Assets: {
      navigationOptions: {
        drawerIcon: ({ tintColor }: { tintColor: string }) => (
          <MaterialIcons
            name="filter-1"
            size={24}
            style={{ color: tintColor }}
          />
        ),
        drawerLabel: 'Assets',
      },
      screen: DrawerScreen,
    },
    Languages: {
      navigationOptions: {
        drawerIcon: ({ tintColor }: { tintColor: string }) => (
          <MaterialIcons
            name="filter-2"
            size={24}
            style={{ color: tintColor }}
          />
        ),
        drawerLabel: 'Languages',
      },
      screen: DrawerScreen,
    },
  },
  {
    drawerPosition: 'left',
  }
)

const RootStack = createStackNavigator(
  {
    MyModal: {
      screen: ModalScreen,
    },
    Main: {
      screen: DrawerMenu,
    },
  },
  {
    mode: 'modal',
    headerMode: 'none',
  }
)

const AppContainer = createAppContainer(RootStack)

export default class App extends React.Component {
  render() {
    return <AppContainer />
  }
}
